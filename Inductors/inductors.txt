Lower value inductor:
-Less DC resistance (less wirings)
-Less impedance (Z(f) = 2πf*L(f))
-Takes up less space(less wirings)
-Needs more output capacitors since current passes faster from the inductor to charges the output.
-More current flow per switching (acts more like a short) -Generates heat

Higher value inductor (Ideally we want to use a higher iductor value):
-More DC resistance (more wirings)
-More impedance (Z(f) = 2πf*L(f)) -Generates heat.
-Takes up more space
-Less current flow per switching due to higher inductance. Acts more like an inductor. Output charges slower.
-Less ripple voltage on the output but if the output has high current transient(s), it will suffer current shortage.
-Needs less output capacitance due to its capability in storing current.
-> Theres also a chance that the inductor/Dc-Dc converter will oscillate on startup if the input current/voltage is low enough, probably due to the High R of the inductor.