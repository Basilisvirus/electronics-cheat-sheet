

.............


/*
select status led color.
RGB or CPY. 
For cpy colors use softwsre PWM so the code needs to stay in that position
to create the color.
*/
void statusLed(char color)//uint32_t tempCounter<600000
{
	if(color == 'r')//red
	{
		BIT_SET(PORTD, BIT(6));//PD6=1
		BIT_CLEAR(PORTC, BIT(3));//PC3=0
	}
	else if(color == 'g')//green
	{
		BIT_CLEAR(PORTD, BIT(6));//PD6=0
		BIT_SET(PORTC, BIT(3));//PC3=1
	}
	else if(color == 'b')//blue
	{
		BIT_CLEAR(PORTD, BIT(6));//PD6=0
		BIT_CLEAR(PORTC, BIT(3));//PC3=0
	}
	else if(color == 'p')//purple
	{
		BIT_CLEAR(PORTC, BIT(3));//PC3=0
		BIT_SET(PORTD, BIT(6));//PD6=1 red
		BIT_CLEAR(PORTD, BIT(6));//PD6=1 blue
	}
	else if(color == 'y')//yellow
	{
		BIT_SET(PORTD, BIT(6));//PD6=1 r
		BIT_CLEAR(PORTC, BIT(3));//PC3=0
		
		BIT_CLEAR(PORTD, BIT(6));//PD6=0 g
		BIT_SET(PORTC, BIT(3));//PC3=1
		
		BIT_CLEAR(PORTC, BIT(3));//PC3=0
		BIT_SET(PORTD, BIT(6));//PD6=1 r
	}
	else if(color == 'c')//cyan
	{
		BIT_CLEAR(PORTD, BIT(6));//PD6=0 g
		BIT_SET(PORTC, BIT(3));//PC3=1
		
		BIT_CLEAR(PORTC, BIT(3));//PC3=0
	}
}
	

		uint32_t tempCounter=0;
		while(1)
		{
			statusLed('r');
			_delay_ms(1000);
			statusLed('g');
			_delay_ms(1000);
			statusLed('b');
			_delay_ms(1000);
			while (tempCounter<600000)
			{
				tempCounter++;
				statusLed('p');
			}
			tempCounter=0;
			
			while (tempCounter<600000)
			{
				tempCounter++;
				statusLed('y');
			}
			tempCounter=0;
			
			while (tempCounter<600000)
			{
				tempCounter++;
				statusLed('c');
			}
			tempCounter=0;
		}
		
		
		.............